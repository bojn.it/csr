# CSR

Certificate Signing Requestor

## Vždy pred použitím...

```
$ apt-get update
$ apt-get dist-upgrade
$ cd /etc/bojn.it/csr
$ ./update
```

## Git Clone

```
$ mkdir /etc/bojn.it
$ cd /etc/bojn.it
$ git clone https://gitlab.com/bojn.it/csr.git csr
```

## Nastavenie predvolených hodnôt

```
$ [cp /etc/bojn.it/vars.cnf.here /etc/bojn.it/vars.cnf]
$ nano /etc/bojn.it/vars.cnf
```

## Preddefinované typy certifikátov

### Identita používateľa

```
$ openssl req -config /etc/bojn.it/csr/openssl-req--create-user-identity-csr.cnf ...
```

### Identita servera

```
$ openssl req -config /etc/bojn.it/csr/openssl-req--create-server-identity-csr.cnf ...
```

## Generovanie požiadavky na podpísanie certifikátu Certifikačnou Autoritou (CA)

### Možnosť č. 1

```
# Vygenerovanie súkromného kľúča (obsahuje v sebe aj verejný kľúč)
# Pre jeho zašifrovanie uveď názov šifrovacieho algoritmu, napr. -aes128
$ openssl genrsa -out /etc/ssl/private/my-key.pem 2048

# Vygenerovanie požiadavky na podpísanie certifikátu CA
$ openssl req -config /etc/bojn.it/csr/openssl-req--create-[[ typ ]]-csr.cnf \
    -new \
    -key /etc/ssl/private/my-key.pem \
    -out /etc/bojn.it/csr/requests/my-csr.pem
```

### Možnosť č. 2

```
# Vygenerovanie nezašifrovaného súkromného kľúča
# (2048 bitov, obsahuje v sebe aj verejný kľúč)
# a požiadavky na podpísanie certifikátu CA
$ openssl req -config /etc/bojn.it/csr/openssl-req--create-[[ typ ]]-csr.cnf \
    -newkey rsa \
    -keyout /etc/ssl/private/my-key.pem \
    -out /etc/bojn.it/csr/requests/my-csr.pem
```

Ak neuvedieš `-keyout`, tak súkromný kľúč bude uložený ako
`/etc/ssl/private/rename-this-key-to-something-meaningful.pem`.

[Dokumentácia "openssl req"](https://www.openssl.org/docs/manmaster/man1/openssl-req.html)

## Overenie platnosti požiadavky na podpísanie certifikátu Certifikačnou Autoritou (CA)

```
$ openssl req -verify -in /etc/bojn.it/csr/requests/my-csr.pem -noout
```

[Dokumentácia "openssl req"](https://www.openssl.org/docs/manmaster/man1/openssl-req.html)

## Zobrazenie obsahu požiadavky na podpísanie certifikátu Certifikačnou Autoritou (CA)

```
$ openssl req -in /etc/bojn.it/csr/requests/my-csr.pem -noout -text
```

[Dokumentácia "openssl req"](https://www.openssl.org/docs/manmaster/man1/openssl-req.html)
